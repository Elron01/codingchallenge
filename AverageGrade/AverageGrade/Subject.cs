﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageGrade
{
    public class Subject : IComparable<Subject>
    {
        public double Grade { get; set; }
        public string Title { get; set; }
        public int LPs { get; set; }
        //public bool IsBachelorThesisGrade => Title.StartsWith("Bachelor");
        public bool IsBachelorThesisGrade()
        {
            return Title.StartsWith("Bachelor");
        }

        public int CompareTo(Subject other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var gradeComparison = Grade.CompareTo(other.Grade);
            if (gradeComparison != 0) return gradeComparison;
            var titleComparison = string.Compare(Title, other.Title, StringComparison.Ordinal);
            if (titleComparison != 0) return titleComparison;
            return LPs.CompareTo(other.LPs);
        }
    }
}
