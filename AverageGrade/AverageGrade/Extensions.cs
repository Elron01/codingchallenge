﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageGrade
{
    public static class Extensions
    {
        public static bool IsADateTime(this DateTime dt, string dateTime)
        {
            var cultureInfo = new CultureInfo("de-DE", true);
            try
            {
                var date = DateTime.Parse(dateTime, cultureInfo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static IEnumerable<IEnumerable<T>> Combinations<T>(this IEnumerable<T> elements, int k)
        {
            return k == 0 ? new[] { new T[0] } :
              elements.SelectMany((e, i) =>
                elements.Skip(i + 1).Combinations(k - 1).Select(c => (new[] { e }).Concat(c)));
        }

        //https://stackoverflow.com/questions/10515449/generate-all-combinations-for-a-list-of-strings
        public static IEnumerable<IEnumerable<T>> Combine<T>(this IEnumerable<T> elements)
        {
            //for (var i = 0; i < elements.Count(); i++)
            //{
            //    yield return ConstructSetFromBits(i).Select(n => elements.ElementAt(n));
            //}
            var enumerable = elements as IList<T> ?? elements.ToList();
            for (var i = 0; i < enumerable.Count; i++)
            {
                yield return ConstructSetFromBits(i).Select(enumerable.ElementAt);
            }
        }

        private static IEnumerable<int> ConstructSetFromBits(int i)
        {
            var n = 0;
            for (; i != 0; i /= 2)
            {
                if ((i & 1) != 0) yield return n;
                n++;
            }
        }
    }
}
