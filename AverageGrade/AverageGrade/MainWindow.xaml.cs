﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AverageGrade
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// http://www.wpftutorial.net/DataGrid.html
    /// </summary>
    public partial class MainWindow : Window
    {
        public const int LpWithoutBa = 90;
        public const int LpWithBa = 102;

        public ObservableCollection<Subject> Subjects { get; set; }

        public ObservableCollection<Subject> UsedSubjects { get; set; }

        public MainWindow() {
            Subjects = new ObservableCollection<Subject>();
            UsedSubjects = new ObservableCollection<Subject>();
            //Subjects.Add(new Subject {Grade = 2d, LPs = 2, Title = "du mich auch"});
            InitializeComponent();
        }

        private void BtStart_Click(object sender, RoutedEventArgs e)
        {
            var text = TbHtmlText.Text;
            if (string.IsNullOrEmpty(text))
            {
                LbState.Content = "Leerer Text...";
                return;
            }
            var list = PlainTextParser.Parse(text);
            var result = GradeCalculator.CalcAverageGrade(list);//, CbInclBa.IsChecked.HasValue ? CbInclBa.IsChecked.Value ? LpWithBa : LpWithoutBa : LpWithoutBa);
            var state = $"Note: {result.Item1}";
            
            Subjects.Clear();
            var lpCount = 0;
            foreach (var subject in result.Item2)
            {
                Subjects.Add(subject);
                lpCount += subject.LPs;
            }
            var leftOvers = list.FindAll(s => !result.Item2.Contains(s));
            foreach (var leftOver in leftOvers)
            {
                UsedSubjects.Add(leftOver);
            }
            state += $", Verrechnete LP: {lpCount}";
            LbState.Content = state;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(TbBachelorGrade.Text) && double.TryParse(TbBachelorGrade.Text, out double grade))
            {
                //Subjects.Add(new Subject{Grade = grade, LPs = 12, Title = "Bachelor Abschluss Arbeit"});
                var items = new List<Subject>();


                var text = TbHtmlText.Text;
                if (string.IsNullOrEmpty(text))
                {
                    LbState.Content = "Leerer Text...";
                    return;
                }
                var list = PlainTextParser.Parse(text);
                list.Add(new Subject { Grade = grade, LPs = 12, Title = "Bachelor Abschluss Arbeit" });
                UsedSubjects.Clear();
                Calculate(list);
            }
            
        }

        private void Calculate(List<Subject> items)
        {
            var result = GradeCalculator.CalcAverageGrade(items);//, CbInclBa.IsChecked.HasValue ? CbInclBa.IsChecked.Value ? LpWithBa : LpWithoutBa : LpWithoutBa);
            var state = $"Note: {result.Item1}";

            Subjects.Clear();
            var lpCount = 0;
            foreach (var subject in result.Item2)
            {
                Subjects.Add(subject);
                lpCount += subject.LPs;
            }
            var leftOvers = items.FindAll(s => !result.Item2.Contains(s));
            foreach (var leftOver in leftOvers)
            {
                UsedSubjects.Add(leftOver);
            }
            state += $", Verrechnete LP: {lpCount}";
            LbState.Content = state;
        }
    }
}
