﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageGrade
{
    public class PlainTextParser
    {
        public static List<Subject> Parse(string text)
        {
            var lines = text.Split('\n').ToList();
            var result = new List<Subject>();
            int buffer;

            //filter out some lines that contains only text
            lines = lines.FindAll(s => s.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).Length >= 4);

            //removes all lines, that doesn't start with a number
            lines = lines.FindAll(s => int.TryParse(s.Split(new char[0], StringSplitOptions.RemoveEmptyEntries)[0], out buffer));

            //remove optional grades
            RemoveOptionalGrades(lines);
            
            //remove all lines, that doesn't end with a date
            lines = lines.FindAll(s => IsADateTime(s.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).ToList().Last()));

            //remove all ungraded lines
            lines = lines.FindAll(s => !s.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).Contains("UB"));



            foreach (var line in lines)
            {



                //http://stackoverflow.com/questions/6111298/best-way-to-specify-whitespace-in-a-string-split-operation
                var wordsInLine = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                
                var semPos = Array.IndexOf(wordsInLine, "SoSe");
                if (semPos < 0) semPos = Array.IndexOf(wordsInLine, "WiSe");
                if (semPos < 0) continue;
                var title = "";
                for (var i = 1; i < semPos; i++)
                {
                    //title += i > 2 ? " " : "" + wordsInLine[i];
                    title += wordsInLine[i] + " ";
                }
                title = title.Trim();
                double grade;
                var gradeString = wordsInLine[semPos + 2].Replace(',', '.');

                //https://msdn.microsoft.com/de-de/library/3s27fasw(v=vs.110).aspx
                const NumberStyles style = NumberStyles.Number; // | NumberStyles.AllowCurrencySymbol;
                var culture = CultureInfo.CreateSpecificCulture("en-GB");

                if (!double.TryParse(gradeString, style, culture, out grade)) continue;
                int lp;
                if (!int.TryParse(wordsInLine[wordsInLine.Length - 3], out lp)) continue;
                result.Add(new Subject {Grade = grade, LPs = lp, Title = title});
            }

            return result;
        }

        private static void RemoveOptionalGrades(List<string> lines)
        {
            var itemsToRemove = new List<string>();
            foreach (var line in lines)
            {
                var words = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                if (IsADateTime(words[words.Length-1]))
                {
                    itemsToRemove.Add(line);
                }
                else
                {
                    break;
                }
            }
            foreach (var line in itemsToRemove)
            {
                lines.Remove(line);
            }
        }
        

        private static bool IsADateTime(string date)
        {

            var cultureInfo = new CultureInfo("de-DE", true);
            try
            {
                var dt = DateTime.Parse(date, cultureInfo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
