﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Combinatorics.Collections;

namespace AverageGrade
{
    public class GradeCalculator
    {
        public static Tuple<double, List<Subject>>  CalcAverageGrade(List<Subject> subjects, double baGrade = 0d)//, int relevantLpCount)
        {
            //var result = 0d;
            //

            var bachelorThesisGrade = subjects.FirstOrDefault(s => s.IsBachelorThesisGrade());
            subjects.Remove(bachelorThesisGrade);
            if (baGrade > 0d)
            {
                bachelorThesisGrade = new Subject{Grade = baGrade, LPs =  12, Title = "Bachelor-Arbeit"};
            }
            //var relevantLpCount = bachelorThesisGrade == null ? 90 : 102;
            var relevantLpCount = 90;

            subjects.RemoveAll(subject => subject.LPs == 0);

            //var sorted = subjects.OrderBy(s => s.Grade).ThenBy(s => s.LPs).ToList();
            var sorted = subjects.OrderBy(s => s.Grade).ToList();

            var gradeLevels = SplitGrades(sorted);


            var gradeSum = 0d;
            var lpSum = 0;
            var resultList = new List<Subject>();

            foreach (var gradeLevel in gradeLevels)
            {
                var lpCount = gradeLevel.Sum(subject => subject.LPs);
                //minimize lp use
                if (lpSum + lpCount > relevantLpCount)
                {
                    var neededLp = relevantLpCount - lpSum;
                    if (neededLp == 0) break;
                    var optimalResult = CalcOptimalLps(gradeLevel, neededLp);
                    resultList.AddRange(optimalResult);

                    break;
                }
                //normal average calculation
                foreach (var subject in gradeLevel)
                {
                    gradeSum += subject.Grade * subject.LPs;
                    lpSum += subject.LPs;
                    //subjectCount++;
                    resultList.Add(subject);
                    //if (lpSum >= relevantLpCount) break;
                }
            }
            if (bachelorThesisGrade != null)
            {
                resultList.Add(bachelorThesisGrade);
                gradeSum += bachelorThesisGrade.Grade * bachelorThesisGrade.LPs;
                lpSum += bachelorThesisGrade.LPs;
            }
            
            return new Tuple<double, List<Subject>>(gradeSum / lpSum, resultList);
        }

        public static List<List<Subject>> SplitGrades(List<Subject> subjects)
        {
            var currentGrade = 0d;
            var result = new List<List<Subject>>();
            var currentGradeLevel = new List<Subject>();
            foreach (var subject in subjects)
            {
                if (subject.Grade > currentGrade)
                {
                    if (currentGradeLevel.Count > 0)
                        result.Add(currentGradeLevel);
                    currentGradeLevel = new List<Subject>();
                    currentGrade = subject.Grade;
                }
                currentGradeLevel.Add(subject);
            }
            return result;
        }

        //private static IEnumerable<Subject> CalcOptimalLps(List<Subject> subjects, int threshold)
        //{
        //    var lpList = subjects.Select(s => s.LPs);
        //    //var combinations = lpList.Combinations(lpList.Count());
        //    var combinations = lpList.Combine();
        //    var bestCombination = new List<int>();
        //    foreach (var combination in combinations)
        //    {
        //        var sum = 0;
        //        var list = combination.ToList();
        //        for (var i = 0; i < list.Count; i++)
        //        {
        //            sum += list.ElementAt(i);
        //            if (sum >= threshold && (bestCombination.Count == 0 || bestCombination.Sum() > sum))
        //            {
        //                bestCombination = list.Take(i + 1).ToList();
        //            }
        //        }
        //    }
        //    //var result = new List<Subject>();
        //    //foreach (var lp in bestCombination)
        //    //{
        //    //    result.Add(subjects.FirstOrDefault(s => s.LPs == lp));
        //    //}
        //    //return result;
        //    return bestCombination.Select(lp => subjects.FirstOrDefault(s => s.LPs == lp));

        //}

        private static IEnumerable<Subject> CalcOptimalLps(List<Subject> subjects, int threshold)
        {
            var permutations = new Permutations<Subject>(subjects.ToArray());
            var lpSum = 0;
            var bestResult = new List<Subject>();
            var currentResult = new List<Subject>();
            //var permutationDone = false;
            foreach (var permutation in permutations)
            {
                foreach (var subject in permutation)
                {
                    currentResult.Add(subject);
                    var currentLpSum = currentResult.Aggregate(0, (s, l) => s + l.LPs);
                    if ( currentLpSum >= threshold)
                    {
                        if (bestResult.Count == 0 || currentLpSum < lpSum)
                        {
                            lpSum = currentLpSum;
                            bestResult = currentResult;
                            currentResult = new List<Subject>();
                            break;
                        }
                    }
                }
                currentResult = new List<Subject>();
            }

            return bestResult;
        }

        //private static 

    }
}
