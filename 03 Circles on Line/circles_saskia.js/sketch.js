function setup() {
  createCanvas(600, 400);
}

var x1 = 100;
var y1 = 100;

var x2 = 5;
var y2 = 3;

var num = 10;

var xLength = x2 - x1;
var yLength = y2 - y1;

var xDist = xLength / (num - 1);
var yDist = yLength / (num - 1);

//var length = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))
//var dis = length / (num - 1)

function draw() {
  for  (var i = 0; i < num; i++) {
    ellipse( x1 + i*xDist, y1 + i*yDist, 10, 10);
  }
}