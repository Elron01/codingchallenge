function Ellipse(x,y,w,h, r,g,b){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.r = r;
    this.g = g;
    this.b = b;
}

var maxX = 600;
var maxY = 400;
var arr = [];

function setup() {
  createCanvas(maxX,maxY);
}

function draw() {
  background(200);
  var w = random(40) + 10;
  var h = random(10, 50);
  var r = random(255);
  var g = random(255);
  var b = random(255);
  var el = new Ellipse(random(w/2, maxX-w/2),random(h/2, maxY-h/2),w,h, r,g,b);
  arr.push(el);
  for (var i = 0; i < arr.length; i++){
    fill(arr[i].r, arr[i].g, arr[i].b);
    ellipse(arr[i].x, arr[i].y, arr[i].w, arr[i].h);
  }
}