var maxX = 1000;
var maxY = 800;
var maxColor = 256;
var circleCount = 200;
var circleX = maxX/2;
var circleY = maxY/2;
var radius = 390;
//global count for the startcircle
var glCount = 0;


function setup() {
  createCanvas(maxX,maxY);
}

function draw() {
	background(200);
		glCount++;
		
	
	//console.log("r: " + r + " ,g: " + g + " ,b: " + b);
	
	var circFactor = 2*Math.PI;
	var angle = circFactor / circleCount
	
	for (var i = 0; i < circleCount; i++){
		// var r = random(maxColor);
		// var g = random(maxColor);
		// var b = random(maxColor);
		// fill(r,g,b);
		var color = (i + (glCount%circleCount))%maxColor;
		console.log("i: " + i + ", glCount: " + glCount + ", color: " + color);
		//fill((i + (glCount%circleCount))%maxColor);
		fill(color);
		//console.log("radius: " + radius + ", angle: " + angle + ", circFactor: " +  circFactor + ", circleCount: " + circleCount);
		var x = radius * Math.cos(angle*(i+1)) + circleX;
		var y = radius * Math.sin(angle*(i+1)) + circleY;
		//console.log("x: " + x + " ,y: " + y);
		ellipse(x,y,10,10);
	}
}