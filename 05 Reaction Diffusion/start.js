//http://karlsims.com/rd.html
//man nimmt ein großes array, welches beide (act und next) aufnimmt und switched dann nur den zeiger zwischen beiden

var maxX = 200;
var maxY = 200;

var diffusionA = 1.0;
var diffusionB = .5;
var feed = .055;
var kill = .062;
var delta = 1.2;

var grid = [];
var next = [];

function setup() {
  createCanvas(maxX,maxY);
  pixelDensity(1);
  grid = [];
  next = [];
  for (var x = 0; x < width; x++) {
    grid[x] = [];
    next[x] = [];
    for (var y = 0; y < height; y++) {
      grid[x][y] = {
        a: 1,
        b: 0
      };
      next[x][y] = {
        a: 1,
        b: 0
      };
    }
  }
  
  //startarea / first reaction
  for (var i = 100; i < 110; i++) {
    for (var j = 100; j < 110; j++) {
      grid[i][j].b = 1;
    }
  }
}

function draw() {
  background(51);
  
  for (var x = 1; x < width - 1; x++) {
    for (var y = 1; y < height - 1; y++) {
      var a = grid[x][y].a;
      var b = grid[x][y].b;
      next[x][y].a = a +
        ((diffusionA * laplaceA(x, y)) -
        (a * b * b) +
        (feed * (1 - a))) * delta;
      next[x][y].b = b +
        ((diffusionB * laplaceB(x, y)) +
        (a * b * b) -
        ((kill + feed) * b)) * delta;

      next[x][y].a = constrain(next[x][y].a, 0, 1);
      next[x][y].b = constrain(next[x][y].b, 0, 1);
    }
  }
  loadPixels();
  for (var x = 0; x < width; x++) {
    for (var y = 0; y < height; y++) {
      var pix = (x + y * width) * 4;
      var a = next[x][y].a;
      var b = next[x][y].b;
      var c = floor((a - b) * 255);
      c = constrain(c, 0, 255);
      pixels[pix + 0] = c;
      pixels[pix + 1] = c;
      pixels[pix + 2] = c;
      pixels[pix + 3] = 255;
    }
  }
  updatePixels();
  swap();
}

function laplaceA(x, y) {
  var sumA = 0;
  sumA += grid[x][y].a * -1;
  if (x > 0) 							sumA += grid[x - 1][y].a * 0.2;
  if (x < width - 1) 					sumA += grid[x + 1][y].a * 0.2;
  if (y < height - 1) 					sumA += grid[x][y + 1].a * 0.2;
  if (y > 0) 							sumA += grid[x][y - 1].a * 0.2;
  if (x > 0 && y > 0) 					sumA += grid[x - 1][y - 1].a * 0.05;
  if (x < width - 1 && y > 0) 			sumA += grid[x + 1][y - 1].a * 0.05;
  if (x < width - 1 && y < height - 1) 	sumA += grid[x + 1][y + 1].a * 0.05;
  if (x > 0 && y < height - 1) 			sumA += grid[x - 1][y + 1].a * 0.05;
  return sumA;
}

function laplaceB(x, y) {
  var sumB = 0;
  sumB += grid[x][y].b * -1;
  if (x > 0) 							sumB += grid[x - 1][y].b * 0.2;
  if (x < width - 1) 					sumB += grid[x + 1][y].b * 0.2;
  if (y < height - 1) 					sumB += grid[x][y + 1].b * 0.2;
  if (y > 0) 							sumB += grid[x][y - 1].b * 0.2;
  if (x > 0 && y > 0) 					sumB += grid[x - 1][y - 1].b * 0.05;
  if (x < width - 1 && y > 0) 			sumB += grid[x + 1][y - 1].b * 0.05;
  if (x < width - 1 && y < height - 1) 	sumB += grid[x + 1][y + 1].b * 0.05;
  if (x > 0 && y < height - 1) 			sumB += grid[x - 1][y + 1].b * 0.05;
  return sumB;
}

function swap() {
  //var temp = grid;
  grid = next;
  //next = temp;
}