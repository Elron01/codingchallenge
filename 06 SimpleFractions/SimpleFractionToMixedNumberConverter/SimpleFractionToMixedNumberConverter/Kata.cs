﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;

namespace SimpleFractionToMixedNumberConverter
{
    public class Kata
    {
        /// <summary>
        /// Mixeds the fraction.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        /// <exception cref="System.DivideByZeroException"></exception>
        public static string MixedFraction(string s)
        {
            var negative = s.Where(c => c == '-').Aggregate(false, (current, c) => !current);

            s = s.Replace("-", "");


            var parts = s.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
            var p = int.Parse(parts[0]);
            var q = int.Parse(parts[1]);
            if (q == 0) throw new DivideByZeroException();

            var a = p / q;
            var p2 = p % q;

            var gcd = Gcd(p2, q);

            if (p == 0) return "0";

            var result = $"{(negative ? "-" : "")}";
            
            if (a != 0)
            {
                result += $"{a}{(p2 != 0 ? " ":"")}";
            }

            return result + $"{(p2 != 0 ? p2 / gcd + "/" + q / gcd : "")}";

        }

        private static int Gcd(int p, int q)
        {
            return q == 0 ? p : Gcd(q, p % q);
        }

        public static int Score(int[] dice)
        {
            const int dotMax = 6;
            var score = 0;

            var tripleValue = new[] {1000, 200, 300, 400, 500, 600};
            var singleValue = new[] { 100, 0, 0, 0, 50, 0 };
            
            var dotCounts = new int[dotMax];

            foreach (var die in dice)
            {
                dotCounts[die-1]++;
            }
            for (var i = 0; i < dotMax; i++)
            {
                score += dotCounts[i] / 3 * tripleValue[i] + dotCounts[i] % 3 * singleValue[i];
            }
            return score;
        }






        //Simple Encryption #2 - Index-Difference

        private static char[] _region;

        public static char[] Region
        {
            get
            {
                if (_region != null) return _region;
                //initialize region
                var list = new List<char>();
                //add upper case letters
                var positions = Enumerable.Range('A', 'Z' - 'A' + 1);
                list.AddRange(positions.Select(c => (char) c));
                //add lower case letters
                positions = Enumerable.Range('a', 'z' - 'a' + 1);
                list.AddRange(positions.Select(c => (char) c));
                //add digits
                positions = Enumerable.Range('0', '9' - '0' + 1);
                list.AddRange(positions.Select(c => (char)c));
                //add special characters 
                list.AddRange(new []{'.',',',':',';','-','?','!', ' ', '\'', '(', ')', '$', '%', '&', '"' });

                _region = list.ToArray();
                return _region;
            }
        }

        public static string Encrypt(string text)
        {
            if (string.IsNullOrEmpty(text)) return text;
            CheckForIllegalLetters(text);

            text = SwitchCases(text);
            text = ReplaceChars(text);
            var encryptedText = MirrorFirstChar(text);

            return encryptedText;
        }

        public static string Decrypt(string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText)) return encryptedText;
            CheckForIllegalLetters(encryptedText);

            encryptedText = MirrorFirstChar(encryptedText);
            encryptedText = ReplaceChars(encryptedText, encrypt:false);
            var decryptedText = SwitchCases(encryptedText);

            return decryptedText;
        }

        private static void CheckForIllegalLetters(string text)
        {
            foreach (var c in text)
            {
                if (!Region.Contains(c)) throw new ArgumentException($"Invalid character: {c}");
            }
        }

        private static string SwitchCases(string text)
        {
            var result = "";
            for (var i = 0; i < text.Length; i++)
            {
                var c = text.ElementAt(i);
                if (i % 2 != 0 && char.IsLetter(c))
                {
                    //check for lower/upper case and then switch
                    c = c >= 'a' ? c.ToString().ToUpper().First() : c.ToString().ToLower().First();
                }
                result += c;
            }
            return result;
        }

        private static string ReplaceChars(string text, bool encrypt = true)
        {
            var result = text.Substring(0, 1);
            for (var i = 1; i < text.Length; i++)
            {
                //for encryption one needs the original predecessor character, for decryption the decrypted predecessor character
                result += ReplaceChar(encrypt ? text.ElementAt(i - 1) : result.Last(), text.ElementAt(i));
            }
            return result;
        }

        private static char ReplaceChar(char predecessor, char currentChar)
        {
            var difference = Array.IndexOf(Region, predecessor) - Array.IndexOf(Region, currentChar);
            difference = difference >= 0 ? difference : difference + Region.Length;
            return Region[difference];
        }

        private static string MirrorFirstChar(string text)
        {
            return Region[Region.Length - 1 - Array.IndexOf(Region, text.ElementAt(0))] + text.Substring(1);
        }



    }
}
