﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class Kata3
    {
        //Convert.ToInt32(hexString, 16)
        public static int FisHex(string name)
        {
            return name.ToLower().Where(c => c >= 'a' && c <= 'f').Aggregate(0, (current, c) => current ^ Convert.ToInt32(c.ToString(), 16));
        }
    }
}
