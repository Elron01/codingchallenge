﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class PiApprox
    {
        public static ArrayList iterPi(double epsilon)
        {
            // your code
            var result = 0d;
            var count = 0;
            var q = 1;
            var diff = Double.MaxValue;
            while (diff > epsilon)
            {
                var addend = (double) 1 / q;
                result = (count % 2 == 0 ? result + addend : result - addend);
                q += 2;
                count++;
                diff = Math.Abs((result * 4) - Math.PI);

                //if (count % 1000000 == 0)
                //{
                //    Console.WriteLine(diff);
                //}

            }

            return new ArrayList{count, Math.Round(result * 4, 10)};
        }
    }
}
