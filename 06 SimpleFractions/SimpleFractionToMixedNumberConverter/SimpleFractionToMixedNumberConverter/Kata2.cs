﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class Kata2
    {
        public List<string> wave(string str)
        {
            var result = new List<string>();
            for (var i = 0; i < str.Length; i++)
            {
                if (!char.IsLetter(str.ElementAt(i))) continue;
                result.Add(str.Substring(0, i) + str.Substring(i,1).ToUpper() + str.Substring(i+1, str.Length - (i + 1)));
            }
            return result;
        }
    }
}
