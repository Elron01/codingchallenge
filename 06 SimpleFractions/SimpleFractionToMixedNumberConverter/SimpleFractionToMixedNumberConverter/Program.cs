﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            //var teststring = "AbCzyx";
            //Console.WriteLine(teststring + " -1-> " + PlayPass.playPass(teststring, 1));

            //Console.WriteLine(teststring + " -2-> " + PlayPass.playPass(teststring, 2));
            //Console.WriteLine(teststring + " -3-> " + PlayPass.playPass(teststring, 3));

            //var i = 2;
            //i = i++ + 2;
            //Console.WriteLine(i);

            //i = 2;
            //i = ++i + 2;
            //Console.WriteLine(i);

            //Console.WriteLine(Kata.Region.Length);
            var text = "!Abc1";
            Console.WriteLine($"{text} -> {Kata.Encrypt(text)} -> {Kata.Decrypt(Kata.Encrypt(text))}");

            text = "0123";
            Console.WriteLine($"{text} -> {Kata.Encrypt(text)} -> {Kata.Decrypt(Kata.Encrypt(text))}");

            text = "abcdefghijklmn";
            Console.WriteLine($"{text} -> {Kata.Encrypt(text)} -> {Kata.Decrypt(Kata.Encrypt(text))}");

            text = "Business";
            Console.WriteLine($"{text} -> {Kata.Encrypt(text)} -> {Kata.Decrypt(Kata.Encrypt(text))}");

            text = "This is a test!";
            Console.WriteLine($"{text} -> {Kata.Encrypt(text)} -> {Kata.Decrypt(Kata.Encrypt(text))}");


            Console.ReadKey();
        }
    }
}
