﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class Runes
    {
        public static int solveExpression(string expression)
        {
            const int errorResult = -1;
            
            var nextNoneDigitIsOperator = false;
            var operatorPos = 0;
            
            var unusedDigit = new List<int>{0,1,2,3,4,5,6,7,8,9};

            //find pos of operator
            for (var i = 0; i < expression.Length; i++)
            {
                var c = expression.ElementAt(i);
                if (char.IsDigit(c) || c == '?')
                {
                    //first digit occurred, next none digit must be the operator
                    nextNoneDigitIsOperator = true;

                    if (c != '?') unusedDigit.Remove(int.Parse(c.ToString()));
                }
                else if (operatorPos == 0 && nextNoneDigitIsOperator)
                {
                    operatorPos = i;
                }
            }
            //parse numbers
            var equalSignPos = expression.IndexOf("=", StringComparison.InvariantCulture);
            var firstOperand = expression.Substring(0, operatorPos);
            var operatorSign = expression.ElementAt(operatorPos);
            var secondOperand = expression.Substring(operatorPos + 1, equalSignPos - (operatorPos + 1));
            var resultOperand = expression.Substring(equalSignPos + 1);
            
            //exclude leading zeros
            if (firstOperand.Length > 1 && firstOperand.StartsWith("?") ||
                secondOperand.Length > 1 && secondOperand.StartsWith("?") ||
                resultOperand.Length > 1 && resultOperand.StartsWith("?"))
            {
                unusedDigit.Remove(0);
            }

            //solve expression
            foreach (var i in unusedDigit)
            {
                var tempresult = 0;
                switch (operatorSign)
                {
                    case '+':
                        tempresult = BuildNumber(firstOperand, i) + BuildNumber(secondOperand, i);
                        break;
                    case '-':
                        tempresult = BuildNumber(firstOperand, i) - BuildNumber(secondOperand, i);
                        break;
                    case '*':
                        tempresult = BuildNumber(firstOperand, i) * BuildNumber(secondOperand, i);
                        break;
                    default:
                        return errorResult;
                }
                if (tempresult == BuildNumber(resultOperand, i))
                    return i;
            }

            return errorResult;
        }

        private static int BuildNumber(string runePart, int digit)
        {
            return int.Parse(runePart.Replace("?", digit.ToString()));
        }
    }
}
