﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class Carboat
    {

        public static string howmuch(int m, int n)
        {
            // your code
            if (m > n)
            {
                var buffer = m;
                m = n;
                n = buffer;
            }

            var money = m;
            var result = "";
            while (money <= n)
            {
                if (money % 9 != 1 || money % 7 != 2)
                {
                    money++;
                    continue;
                }
                result += $"[M: {money} B: {money / 7} C: {money / 9}]";
                money++;
            }

            return "[" + result + "]";
        }
    }
}
