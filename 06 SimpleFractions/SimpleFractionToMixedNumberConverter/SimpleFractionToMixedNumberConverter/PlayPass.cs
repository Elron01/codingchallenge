﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class PlayPass
    {
        public static string playPass(string s, int n)
        {
            //var temp1 = "";
            //foreach (var c in s.ToLower())
            //{
            //    //temp1 = (char.IsLetter(c) ? (c - 'a' + n) % ('z' - 'a' + 1) + temp1.Length % 2 == 0 ? 'A' : 'a' : char.IsDigit(c) ? ((char)(9 - int.Parse(c.ToString()))) : c) + temp1;
            //    if (char.IsLetter(c))
            //    {
            //        temp1 = (char)((c - 'a' + n) % ('z' - 'a' + 1) + (temp1.Length % 2 == 0 ? 'A' : 'a')) + temp1;
            //    }
            //    else if (char.IsDigit(c))
            //    {
            //        temp1 = 9 - int.Parse(c.ToString()) + temp1;
            //    }
            //    else
            //    {
            //        temp1 = c + temp1;
            //    }
            //}

            //return temp1;

            return s.ToLower().Aggregate("", (current, c) => (char.IsLetter(c) ? (char) ((c - 'a' + n) % ('z' - 'a' + 1) + (current.Length % 2 == 0 ? 'A' : 'a')) : char.IsDigit(c) ? (char) (9 - int.Parse(c.ToString()) + '0') : c) + current);
        }
    }
}
