﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter
{
    public class Line
    {
        public static string WhoIsNext(string[] names, long n)
        {
            var namesCount = names.Length;
            var multiplier = 1;
            var personCount = 0;
            while (personCount + multiplier * namesCount < n)
            {
                personCount += multiplier * namesCount;
                multiplier *= 2;
            }
            //return names[personCount == 0 ? (((n - personCount) / (multiplier)) ) - 1 : (n - personCount) / (multiplier)];
            return names[(n - personCount) / multiplier - personCount == 0 ? 1 : 0];
        }

        //public static string WhoIsNext(string[] names, long n)
        //{
        //    var namesCount = names.Length;
        //    var counter = 0;
        //    var minPos = 0;
        //    var maxPos = 0;
        //    while (maxPos < n)
        //    {
        //        minPos = maxPos;
        //        maxPos += ++counter * namesCount;
        //    }
        //    var actpos = minPos;
        //    var namePos = 0;
        //    while (actpos + counter < n)
        //    {
        //        namePos++;
        //        actpos += counter;
        //    }

        //    return names[namePos];
        //}
    }
}
