﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleFractionToMixedNumberConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter.Tests
{
    [TestClass()]
    public class LineTests
    {
        [TestMethod()]
        public void Test1()
        {
            string[] names = new string[] { "Sheldon", "Leonard", "Penny", "Rajesh", "Howard" };
            int n = 1;
            Assert.AreEqual("Sheldon", Line.WhoIsNext(names, n));
        }

        [TestMethod]
        public void Test2()
        {
            string[] names = new string[] { "Sheldon", "Leonard", "Penny", "Rajesh", "Howard" };
            int n = 6;
            Assert.AreEqual("Sheldon", Line.WhoIsNext(names, n));
        }

        [TestMethod]
        public void Test3()
        {
            string[] names = new string[] { "Sheldon", "Leonard", "Penny", "Rajesh", "Howard" };
            var k = 1;
            for (var i = 1; i <= 30; i*=2)
            {
                for (var l = 0; l < names.Length; l++)
                {
                    for (var j = 0; j < i; j++)
                    {
                        Assert.AreEqual(names[l], Line.WhoIsNext(names, k++), $"k:{k}");
                    }
                }
            }
            //Assert.AreEqual("Sheldon", Line.WhoIsNext(names, n));
        }
    }
}