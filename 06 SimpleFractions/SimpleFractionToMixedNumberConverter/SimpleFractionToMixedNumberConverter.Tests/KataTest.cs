// <copyright file="KataTest.cs">Copyright ©  2017</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleFractionToMixedNumberConverter;

namespace SimpleFractionToMixedNumberConverter.Tests
{
    /// <summary>Diese Klasse enthält parametrisierte Komponententests für Kata.</summary>
    [PexClass(typeof(Kata))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClassAttribute]
    public partial class KataTest
    {
        /// <summary>Test-Stub für MixedFraction(String)</summary>
        [PexMethod]
        public string MixedFractionTest(string s)
        {
            string result = Kata.MixedFraction(s);
            return result;
            // TODO: Assertionen zu Methode KataTest.MixedFractionTest(String) hinzufügen
        }

        [TestMethod]
        public void CheckCodeWarTestCases()
        {
            Assert.AreEqual("4 2/3", Kata.MixedFraction("42/9"));
            Assert.AreEqual("2", Kata.MixedFraction("6/3"));
            Assert.AreEqual("1", Kata.MixedFraction("1/1"));
            Assert.AreEqual("1", Kata.MixedFraction("11/11"));
            Assert.AreEqual("2/3", Kata.MixedFraction("4/6"));
            Assert.AreEqual("0", Kata.MixedFraction("0/18891"));
            Assert.AreEqual("-1 3/7", Kata.MixedFraction("-10/7"));
            Assert.AreEqual("3 1/7", Kata.MixedFraction("-22/-7"));
            Assert.AreEqual("-195595/564071", Kata.MixedFraction("-195595/564071"));

            //Assert.Throws(typeof(DivideByZeroException), delegate { Kata.MixedFraction("0/0"); });
            //Assert.Throws(typeof(DivideByZeroException), delegate { Kata.MixedFraction("3/0"); });
            try
            {
                Kata.MixedFraction("0/0");
                Assert.Fail();
            }
            catch (DivideByZeroException ex)
            {
                var msg = ex.ToString();
            }

            try
            {
                Kata.MixedFraction("3/0");
                Assert.Fail();
            }
            catch (DivideByZeroException) { }
        }

        [TestMethod]
        public static void ShouldBeWorthless()
        {
            Assert.AreEqual(0, Kata.Score(new int[] { 2, 3, 4, 6, 2 }), "Should be 0 :-(");
        }

        [TestMethod]
        public static void ShouldValueTriplets()
        {
            Assert.AreEqual(400, Kata.Score(new int[] { 4, 4, 4, 3, 3 }), "Should be 400");
        }

        [TestMethod]
        public static void ShouldValueMixedSets()
        {
            Assert.AreEqual(450, Kata.Score(new int[] { 2, 4, 4, 5, 4 }), "Should be 450");
        }

        [TestMethod]
        public void EncryptExampleTests()
        {
            Assert.AreEqual("$-Wy,dM79H'i'o$n0C&I.ZTcMJw5vPlZc Hn!krhlaa:khV mkL;gvtP-S7Rt1Vp2RV:wV9VuhO Iz3dqb.U0w", Kata.Encrypt("Do the kata \"Kobayashi-Maru-Test!\" Endless fun and excitement when finding a solution!"));
            Assert.AreEqual("5MyQa9p0riYplZc", Kata.Encrypt("This is a test!"));
            Assert.AreEqual("5MyQa79H'ijQaw!Ns6jVtpmnlZ.V6p", Kata.Encrypt("This kata is very interesting!"));
        }

        [TestMethod]
        public void DecryptExampleTests()
        {
            Assert.AreEqual("Do the kata \"Kobayashi-Maru-Test!\" Endless fun and excitement when finding a solution!", Kata.Decrypt("$-Wy,dM79H'i'o$n0C&I.ZTcMJw5vPlZc Hn!krhlaa:khV mkL;gvtP-S7Rt1Vp2RV:wV9VuhO Iz3dqb.U0w"));
            Assert.AreEqual("This is a test!", Kata.Decrypt("5MyQa9p0riYplZc"));
            Assert.AreEqual("This kata is very interesting!", Kata.Decrypt("5MyQa79H'ijQaw!Ns6jVtpmnlZ.V6p"));
        }

        [TestMethod]
        public void EmptyTests()
        {
            Assert.AreEqual("", Kata.Encrypt(""));
            Assert.AreEqual("", Kata.Decrypt(""));
        }

        [TestMethod]
        public void NullTests()
        {
            Assert.AreEqual(null, Kata.Encrypt(null));
            Assert.AreEqual(null, Kata.Decrypt(null));
        }
    }
}
