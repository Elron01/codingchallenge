﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleFractionToMixedNumberConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter.Tests
{
    [TestClass()]
    public class CarboatTests
    {
        [TestMethod()]
        public static void BasicTest()
        {
            Assert.AreEqual("[[M: 37 B: 5 C: 4][M: 100 B: 14 C: 11]]", Carboat.howmuch(1, 100));
            Assert.AreEqual("[]", Carboat.howmuch(2950, 2950));
        }
    }
}