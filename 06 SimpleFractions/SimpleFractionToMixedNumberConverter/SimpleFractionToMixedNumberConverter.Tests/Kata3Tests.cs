﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleFractionToMixedNumberConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter.Tests
{
    [TestClass()]
    public class Kata3Tests
    {
        [TestMethod()]
        public void BasicTests()
        {
            Assert.AreEqual(1, Kata3.FisHex("pufferfish"));
            Assert.AreEqual(14, Kata3.FisHex("puffers"));
            Assert.AreEqual(14, Kata3.FisHex("balloonfish"));
            Assert.AreEqual(4, Kata3.FisHex("blowfish"));
            Assert.AreEqual(10, Kata3.FisHex("bubblefish"));
            Assert.AreEqual(10, Kata3.FisHex("globefish"));
            Assert.AreEqual(1, Kata3.FisHex("swellfish"));
            Assert.AreEqual(8, Kata3.FisHex("toadfish"));
            Assert.AreEqual(9, Kata3.FisHex("toadies"));
            Assert.AreEqual(9, Kata3.FisHex("honey toads"));
            Assert.AreEqual(13, Kata3.FisHex("sugar toads"));
            Assert.AreEqual(5, Kata3.FisHex("sea squab"));
        }
        
    }
}