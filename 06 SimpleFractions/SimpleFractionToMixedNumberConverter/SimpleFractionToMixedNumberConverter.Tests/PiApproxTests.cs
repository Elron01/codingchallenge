﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleFractionToMixedNumberConverter;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFractionToMixedNumberConverter.Tests
{
    [TestClass()]
    public class PiApproxTests
    {
        [TestMethod()]
        public void Test1()
        {
            ArrayList r = PiApprox.iterPi(0.1);
            ArrayList c = new ArrayList { 10, 3.0418396189 };
            Assert.AreEqual(c, r);
        }
    }
}